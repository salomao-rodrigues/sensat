import { css } from '@emotion/core'

export const globalStyles = css({
  html: {
    fontFamily: "font-family: 'Open Sans', sans-serif",
  }
})

export const root = css({
  padding: '1rem'
})
