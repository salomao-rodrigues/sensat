const fs = require('fs')
const express = require('express')
const cors = require('cors')
const app = express()
const port = 8080

var file = fs.readFileSync(__dirname + '/sensor_readings.json', 'utf8')
const data = JSON.parse(file)

app.get('/', cors({ origin: 'http://localhost:3000' }), (_, res) =>
  res.json(data)
)

app.listen(port, () => console.log(`Server listening on port ${port}!`))
