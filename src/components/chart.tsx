import React from 'react'
import { XAxis, YAxis, ResponsiveContainer, ScatterChart, Scatter } from 'recharts'

type Props = {
  data: any
}

const Chart: React.FC<Props> = ({ data }) => {
  const chartData = [
    { reading: 14, 'reading_ts': 1503617297689 },
    { reading: 15, 'reading_ts': 1503616962277 },
    { reading: 15, 'reading_ts': 1503616882654 },
    { reading: 20, 'reading_ts': 1503613184594 },
    { reading: 15, 'reading_ts': 1503611308914 },
  ]
  
  return (
    <ResponsiveContainer width='95%' height={500} >
      <ScatterChart>
        <XAxis
          dataKey='reading_ts'
          domain={['auto', 'auto']}
          name='Time'
          // tickFormatter={(unixTime) => moment(unixTime).format('HH:mm Do')}
          tickFormatter={timeStr => new Date(timeStr).toUTCString()}
          type='number'
        />
        <YAxis dataKey='reading' name='Value' />

        <Scatter
          data={data.slice(0, 100)}
          line={{ stroke: '#eee' }}
          lineJointType='monotoneX'
          lineType='joint'
          name='Values'
        />

      </ScatterChart>
    </ResponsiveContainer>
  )
}

export default Chart
