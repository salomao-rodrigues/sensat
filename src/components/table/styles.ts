import { css } from '@emotion/core'

export const table = css({
  'tr:nth-of-type(2n)': {
    backgroundColor: '#eee'
  }
})

export const cell = css({
  border: '1px solid rgb(238, 238, 238)',
  flex: '1',
  padding: '0.2rem 0.4em',
  whiteSpace: 'nowrap'
})

export const header = css({
  backgroundColor: '#ccc'
})

export const headerCell = css([
  cell,
  {
    cursor: 'pointer'
  }
])
