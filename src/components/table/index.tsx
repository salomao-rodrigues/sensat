/** @jsx jsx */
import { Fragment } from 'react'
import { jsx } from '@emotion/core'
import { useState } from 'react'
import * as st from './styles'

const NUM_ROWS = 10

export type Columns<T> = Record<keyof T, string>
type Formatter = (arg: any) => any

type Props<T> = {
  columns: Columns<T>
  data: T[]
  formatter?: Formatter
}

type Order<T> = {
  key?: keyof T
  type: number // 1 = Ascending and -1 = Descending
}

const identity: Formatter = x => x

export default function Table<T>({
  columns,
  data: initialData,
  formatter = identity
}: Props<T>) {
  const [page, setPage] = useState(0)
  const [order, setOrder] = useState<Order<T>>({ type: 1 })
  const totalPages = Math.floor(initialData.length / NUM_ROWS) - 1
  const start = page * NUM_ROWS
  const [data, setData] = useState(initialData)
  const pagedData = data.slice(start, start + NUM_ROWS)

  const compareBy = ({ key, type }: Order<T>) => (a: T, b: T) => {
    if (!key) return 0
    if (a[key] < b[key]) return -type
    if (a[key] > b[key]) return type
    return 0
  }

  const sortBy = (key: keyof T) => {
    const newOrder = {
      key,
      type: key === order.key ? -order.type : 1
    }
    setOrder(newOrder)

    const dataClone = [...data]
    dataClone.sort(compareBy(newOrder))
    setData(dataClone)
  }

  const orderArrow = (key: keyof T) => {
    if (key === order.key) {
      return order.type === 1 ? '↑' : '↓'
    }

    return null
  }

  return (
    <Fragment>
      <table css={st.table} cellSpacing={0}>
        <thead>
          <tr css={st.header}>
            {(Object.entries(columns) as Array<[keyof T, string]>).map(
              ([key, label]: [keyof T, string]) => (
                <th
                  key={`${key}`}
                  css={st.headerCell}
                  onClick={() => sortBy(key)}
                >
                  {label} {orderArrow(key)}
                </th>
              )
            )}
          </tr>
        </thead>
        <tbody>
          {pagedData.map((dataRow, index) => (
            <tr key={index}>
              {Object.entries(dataRow).map(([key, value]) => (
                <td key={key} css={st.cell}>
                  {formatter(value)}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <p>
        Page{' '}
        <input
          onChange={({ target }) => setPage(Number(target.value) - 1)}
          value={page + 1}
        />{' '}
        of {totalPages + 1}
      </p>
      <p>
        {page > 0 && (
          <button onClick={() => setPage(page - 1)}>Previous</button>
        )}
        {page < totalPages && (
          <button onClick={() => setPage(page + 1)}>Next</button>
        )}
      </p>
    </Fragment>
  )
}
