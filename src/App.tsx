/** @jsx jsx */
import { useEffect, useState, Fragment } from 'react'
import { Global, jsx } from '@emotion/core'

import * as st from './styles'
import Table, { Columns } from './components/table'
import Chart from './components/chart'

type SensorReading = {
  id: string
  box_id: string
  sensor_type: string // TODO: ensure is a known sensor type
  unit: string // TODO: ensure is a known unit
  name: string // TODO: ensure is a sensor name
  range_l: number
  range_u: number
  longitude: number
  latitude: number
  reading: number
  reading_ts: string
}

const columns: Columns<SensorReading> = {
  'id': 'ID',
  'box_id': 'Box ID',
  'sensor_type': 'Sensor Type',
  'unit': 'Unit',
  'name': 'Name',
  'range_l': 'Lower Bound',
  'range_u': 'Upper Bound ',
  'longitude': 'Longitude',
  'latitude': 'Latitude',
  'reading': 'Reading',
  'reading_ts': 'Time'
}

const App = () => {
  const [sensorData, setSensorData] = useState<SensorReading[]>()
  useEffect(() => {
    fetch('http://localhost:8080')
      .then(response => response.json())
      .then(setSensorData)
  }, [])

  return (
    <div css={st.root}>
      <Global styles={st.globalStyles} />
      <header>
        <h1>SenSat</h1>
      </header>
      {sensorData ? <Fragment>
        <Table columns={columns} data={sensorData} />
        {/* <Chart data={sensorData} /> */}
      </Fragment>
      : 'No data'}
    </div>
  )
}

export default App
